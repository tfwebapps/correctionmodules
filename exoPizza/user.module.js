const users = [ 
    { id : 1, name : "Théophil Defer", email : "theo.df@gmail.com", phone : "0472727272", adresse : "rue de la rue, 1111 oui"  },
    { id : 2, name : "Anne Onyme", email : "anne.o@gmail.com", phone : "0471717171", adresse : "avenue de l'avenue, 1111 oui"  },
]

let currentUserId = 2

const createUser = (name, email, phone, adresse) => {    
    const userToAdd = { id : ++currentUserId, name, email, phone, adresse }
    users.push(userToAdd)
    return userToAdd
}

const getUser = (id) => {
    return users.find(u => u.id === id)
}

const updateUser = (id, user) => {
    const findIndex = users.findIndex(u => u.id === id)
    if(findIndex !== -1) {
        users[findIndex] = {...users[findIndex], ...user}
        return users[findIndex]
    }
    return null
}

const deleteUser = (id) => {
    const findIndex = users.findIndex(u => u.id === id)
    if(findIndex !== -1 ) {
        users.splice(findIndex, 1)
    }
 }

const getUsers = () => {
    return users
}

module.exports = {createUser, getUser, updateUser, deleteUser, getUsers}