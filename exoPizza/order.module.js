const orders = [
    { id : 1, userId : 2 , pizzas : [ { pizzaId : 1, quantity : 1} , { pizzaId : 2, quantity : 2 } ] },
    { id : 2, userId : 2 , pizzas : [ { pizzaId : 3, quantity : 2} ] },
]

let currentOrderId = 1


const pizzaDB = require("./pizzeria.module")
const { getUser } = require("./user.module")

const displayMenu = () => {
    console.log(pizzaDB.getAll())
}

const orderPizza = (userId, pizzas) => {
    const orderToAdd = {
        id : ++currentOrderId,
        userId,
        pizzas
    }
    orders.push(orderToAdd)
    return orderToAdd

}

const getById = (id) => {
    return orders.find(o => o.id === id)
}

const calculateOrder = (id) => {
    const order = getById(id)

    // let totalPrice = 0
    // order.pizzas.forEach(p => { 
    //     totalPrice += pizzaDB.getById(p.pizzaId).price * p.quantity
    // })

    const totalPrice = order.pizzas
                                .map(p => pizzaDB.getById(p.pizzaId).price * p.quantity)
                                .reduce((totalPrice, p) => totalPrice + p )
    
    return totalPrice
}

const calculateTotalOrders = (userId) => {
    const ordersForUserId = orders.filter(o => o.userId === userId)
    const userOrders = ordersForUserId.reduce((acc, order) => {
        acc.user = getUser(order.userId)
        order.pizzas.forEach(p => {
            acc.pizzas.push({...pizzaDB.getById(p.pizzaId), quantity : p.quantity})
            acc.total += pizzaDB.getById(p.pizzaId).price * p.quantity
        })
        return acc
    }, { user : {}, pizzas : [], total : 0 })

    userOrders.total = +userOrders.total.toFixed(2)
    return userOrders
}

module.exports = { displayMenu, orderPizza, calculateOrder, calculateTotalOrders }