const pizzas = [
    { id: 1, name: "Margherita", price: 8.50 },
    { id: 2, name: "4 fromages", price: 11.50 },
    { id: 3, name: "Parmigiana", price: 12.99 },
]

let currentPizzaID = 3

const pizzaDB = {

    getAll: () => {
        return pizzas
    },

    getById: (id) => {
        return pizzas.find(p => p.id === id)
    }
}

module.exports = pizzaDB