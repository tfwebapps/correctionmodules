
//jour avant Christmas
//jour avant Birthday
//jour avant Vacations
const TODAY = new Date()
const TODAY_YEAR = TODAY.getFullYear()

const getNextDate = (baseDate) => {
    if( (baseDate.getMonth() < TODAY.getMonth()) ||
     (baseDate.getMonth() === TODAY.getMonth() && baseDate.getDate() > TODAY.getDate()) ) {
        return new Date(baseDate.getFullYear() + 1, baseDate.getMonth(), baseDate.getDate())
    }
    else return baseDate
}

const diffDate = (dateToCompare) => {
    const daysInMilli = TODAY - dateToCompare
    return Math.ceil(Math.abs((daysInMilli / (1000 * 3600 * 24))))
}

const daysBefore = {
    christmas : () => {
        const christmasDate = getNextDate(new Date(TODAY_YEAR, 11, 25))
        if(TODAY.getFullYear() == christmasDate.getFullYear() && TODAY.getMonth() == christmasDate.getMonth() && TODAY.getDate() == christmasDate.getDate()) {
             console.log("Joyeux Noël !!!");
        } else {
            days = diffDate(christmasDate)
            console.log(`Il reste ${days} jours avant Noël`);
        }
        // if(TODAY.getMonth == 11 && TODAY.getDate > 25) {
        //     christmasDate.setFullYear(TODAY_YEAR + 1)
        // } 
    },

    birthday : (someBirthday) => {
        someBirthday.setFullYear(TODAY_YEAR)
        const birthday = getNextDate(someBirthday)
        if(TODAY.getFullYear() == birthday.getFullYear() && TODAY.getMonth() == birthday.getMonth() && TODAY.getDate() == birthday.getDate()) {
            console.log("Ouaiiiis ! Joyeux anniversaire !!");
        } else {
            days = diffDate(birthday)
            console.log(`Il reste ${days} jours avant l'anniversaire suivant ${birthday.toLocaleDateString('fr-BE')}`);
        }
    },

    vacations : () => {
        const startVac = new Date(TODAY_YEAR, 6, 1)
        const endVac = new Date(TODAY_YEAR, 7, 31)
        //Si on est en plein pendant les vacances
        if(TODAY.getMonth() === startVac.getMonth() || TODAY.getMonth() === endVac.getMonth()) {
            console.log("Bonnes vacances");    
            const days = diffDate(endVac)        
            console.log(`Il reste ${days} jours avant la reprise :'( )}`);
            return
        }
        if( TODAY.getMonth() > endVac.getMonth ) {
            startVac.setFullYear(TODAY_YEAR + 1)
        }

        const days = diffDate(startVac)
        console.log(`Il reste ${days} jours avant le début des vacances`);
    }
}

module.exports = daysBefore
